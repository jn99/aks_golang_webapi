FROM golang:latest

RUN mkdir /build
WORKDIR /build

RUN export GO111MODULE=on
RUN go install gitlab.com/jn99/aks_golang_webapi@latest
#RUN cd /build && git clone https://gitlab.com/jn99/aks_golang_webapi.git
